//ARRAY CHISTES PARA EL TIMER PROMPT
const chistes = ['Ley del vago nº 30291884792, eres tan vago k no te has leído el num completo.',
'Cariño, creo que estás obsesionado con el fútbol y me haces falta ¡¿Qué falta?! ¡¿Qué falta?! ¡¡Si no te he tocado!!',
'Si car es carro y men es hombre entonces Carmen es un transformer...', '¿Cuanto cuesta esta estufa? / 5.000€ / Pero, oiga, esto es una estafa / No señor, es una estufa',
'¡Soldado López! / ¡Sí, mi capitán! / No lo vi ayer en la prueba de camuflaje. / ¡Gracias, mi capitán!','¿Cómo se despiden los químicos?  Ácido un placer...',
'Como maldice un pollito a otro pollito?   ¡Caldito seas!', ' ¡A mí nadie me da órdenes! /  "2% de batería. Conecte el cargador"/ Voy.',
'¿Cuál es la diferencia entre un motor y un inodoro? /En que en el motor tu te sientas para correr, y en el inodoro tu corres para sentarte.']
//ARRAY CHISTES PARA EL TIMER PROMPT

// TIMER.PROMPT-------------------------
// setInterval(function(){
//     chistes.sort(() => 0.5 - Math.random())
    
//     alert(chistes[0])
// }, 25000);
// TIMER.PROMPT-------------------------



// BUTTONS ---------------------------------------------------
const mainLogo$$ = document.querySelector('#logo');
const footerLogo$$ = document.querySelector('#logo1');
const listBtn$$ = document.querySelector('#btnList');
const addBtn$$ = document.querySelector('#btnAdd');
const aboutBtn$$ = document.querySelector('#btnAboutMe');
const userBtn$$ = document.querySelector('#btnUser');
const gameBtn$$ = document.querySelector('#btnGame');
// BUTTONS ---------------------------------------------------


// ADD LIST CONTENT TO-MEME VIEW
function addContent(){
        const listViewDiv$$ = document.querySelector('.list__content');

    memes.forEach(meme  => {
        //MEMES EN VIEW LIST
        const newDiv$$ = document.createElement('div');
        newDiv$$.classList.add('tarjetaMemes');
        newDiv$$.innerHTML= `
            <div class="polaroid">
                    <div class="tarjetaMemes__img">
                            <img src="${meme.imgUrl}">
                    </div>

                    <div class="tarjetaMemes__title">
                    <p>${meme.title}</p>
                    
                    </div>
                    
                    </div>
                    `
            listViewDiv$$.appendChild(newDiv$$); 
                    
                    //DETAIL VIEW
                    const memesImgDetail$$ = document.querySelectorAll('.tarjetaMemes__img')
                    memesImgDetail$$[meme.id].addEventListener('click', () => {
                                document.querySelector('.list').classList.add('displayNone');
                                document.querySelector('.main').classList.add('displayNone');
                                document.querySelector('.add').classList.add('displayNone');
                                document.querySelector('.about').classList.add('displayNone');
                                document.querySelector('.detail').classList.remove('displayNone');
                                document.querySelector('.user').classList.add('displayNone');
                                document.querySelector('[data-fn="header"]').classList.add('displayNone');
                                document.querySelector('[data-fn="footer"]').classList.add('displayNone');
                                document.querySelector('.game').classList.add('displayNone');

                    // CERRAR VISTA DETALLE-------------------------------------
                    const CloseDetail$$ = document.createElement('div');
                    CloseDetail$$.classList.add('detail_close_btn');
                    CloseDetail$$.innerHTML = `<img src="./Iconos/x.svg">`;
                    
                    CloseDetail$$.addEventListener('click', () =>{
                                document.querySelector('.list').classList.remove('displayNone');
                                document.querySelector('.main').classList.add('displayNone');
                                document.querySelector('.add').classList.add('displayNone');
                                document.querySelector('.about').classList.add('displayNone');
                                document.querySelector('.detail').classList.add('displayNone');
                                document.querySelector('.user').classList.add('displayNone');
                                document.querySelector('[data-fn="header"]').classList.remove('displayNone');
                                document.querySelector('[data-fn="footer"]').classList.remove('displayNone');
                                document.querySelector('.game').classList.add('displayNone');     
                                newDetail$$.remove()
                                CloseDetail$$.remove()      
                    })
                        
                    //VISTA DETALLE INSERTO MEME (FOREACH_MEMES)-------------------------------
                    const detailViewDiv$$ = document.querySelector('.detail');
                    const newDetail$$ = document.createElement('div');
                    
                    newDetail$$.classList.add('tarjetaMemes--big');
                    newDetail$$.innerHTML= `
                                <div class="polaroidBig">
                                        <div class="tarjetaMemes__imgBig">
                                                <img src="${meme.imgUrl}">
                                        </div>
                        
                                        <div class="tarjetaMemes__titleBig">
                                            <h2>${meme.title}</h2>
                                            <p>${meme.description}</p>
                        
                                        </div>
                        
                                </div>`

                    detailViewDiv$$.appendChild(CloseDetail$$);
                    detailViewDiv$$.appendChild(newDetail$$);
                    })
           

            //AÑADIMOS BOTÓN PARA FAVORITOS (FOREACH_MEMES)
            const divTarjetaMemes$$ = document.querySelectorAll('.tarjetaMemes__title');
            const newBtnAdd$$ = document.createElement('div')
            newBtnAdd$$.setAttribute('idMeme', meme.id);
            newBtnAdd$$.classList.add('button');
            newBtnAdd$$.innerHTML = `<img src="./Iconos/like.svg">`;
            
            newBtnAdd$$.addEventListener('click', function addFavMemes(){   
                
                            newBtnAdd$$.innerHTML = `<img src="./Iconos/likeRed.svg">`;    
                            const addViewDiv$$ = document.querySelector('.add__content');
                            const newAdd$$ = document.createElement('div');
                            newAdd$$.classList.add('tarjetaMemes');
                            newAdd$$.innerHTML= `
                                    <div class="polaroid">
                                            <div class="tarjetaMemes__img">
                                                    <img src="${meme.imgUrl}">
                                            </div>

                                            <div class="tarjetaMemes__title">
                                                <p>${meme.title}</p>
                                            
                                            </div>
                                            
                                    </div>
                                `
                                addViewDiv$$.appendChild(newAdd$$);
                                newBtnAdd$$.removeEventListener('click', addFavMemes);
                                newBtnAdd$$.addEventListener('click', function deleteFavMemes(){  
                                    newAdd$$.remove();
                                    newBtnAdd$$.innerHTML = `<img src="./Iconos/like.svg">`;
                                    newBtnAdd$$.addEventListener('click',addFavMemes);
                                })
            })
            divTarjetaMemes$$[meme.id].appendChild(newBtnAdd$$);
     })                  
}
               
// CURRENT VIEW FOR CURRENT VIEW --
const state = {currentView:'main'};
// CURRENT VIEW FOR CURRENT VIEW --
                
// VIEW MAIN---------------------------------------------------
const viewMain = () => {
    if(state.currentView === 'main'){
        window.alert("YA ESTAS EN MEME HOME.");
    } else {
        state.currentView = 'main';
        document.querySelector('.list').classList.add('displayNone');
        document.querySelector('.main').classList.remove('displayNone');
        document.querySelector('.add').classList.add('displayNone');
        document.querySelector('.about').classList.add('displayNone');
        document.querySelector('.detail').classList.add('displayNone');
        document.querySelector('.user').classList.add('displayNone');
        document.querySelector('.game').classList.add('displayNone');
    }
};
// VIEW MAIN---------------------------------------------------

// VIEW LIST----------------------------------------------------
const viewList = () =>{
    if(state.currentView == 'list'){
        window.alert("YA ESTAS EN MEME's.");
    } else {
        state.currentView = 'list';
        document.querySelector('.list').classList.remove('displayNone');
        document.querySelector('.main').classList.add('displayNone');
        document.querySelector('.add').classList.add('displayNone');
        document.querySelector('.about').classList.add('displayNone');
        document.querySelector('.detail').classList.add('displayNone');
        document.querySelector('.user').classList.add('displayNone');
        document.querySelector('.game').classList.add('displayNone');
     }
};                
// VIEW LIST---------------------------------------------------------

// VIEW ADD---------------------------------------------------------
const viewAdd = () => {
    if(state.currentView == 'add'){
        window.alert("YA ESTAS EN FAV's");
    } else {
        state.currentView = 'add';
        document.querySelector('.list').classList.add('displayNone');
        document.querySelector('.main').classList.add('displayNone');
        document.querySelector('.add').classList.remove('displayNone');
        document.querySelector('.about').classList.add('displayNone');
        document.querySelector('.detail').classList.add('displayNone');
        document.querySelector('.user').classList.add('displayNone');
        document.querySelector('.game').classList.add('displayNone');
    }
};

// VIEW ADD---------------------------------------------------------

// VIEW ABOUT-------------------------------------------------------
const viewAbout = () => {
    if(state.currentView == 'about'){
        window.alert("YA ESTAS EN ABOUT ME(ME)");
    } else {
        state.currentView = 'about';
        document.querySelector('.list').classList.add('displayNone');
        document.querySelector('.main').classList.add('displayNone');
        document.querySelector('.add').classList.add('displayNone');
        document.querySelector('.about').classList.remove('displayNone');
        document.querySelector('.detail').classList.add('displayNone');
        document.querySelector('.user').classList.add('displayNone');
        document.querySelector('.game').classList.add('displayNone');
    }
};
// VIEW ABOUT-------------------------------------------------------

// VIEW USER-------------------------------------------------------
const viewUser = () => {
    if(state.currentView == 'user'){
        window.alert("YA ESTAS EN USUARIO");
    } else {
        state.currentView = 'user';
        document.querySelector('.list').classList.add('displayNone');
        document.querySelector('.main').classList.add('displayNone');
        document.querySelector('.add').classList.add('displayNone');
        document.querySelector('.about').classList.add('displayNone');
        document.querySelector('.detail').classList.add('displayNone');
        document.querySelector('.user').classList.remove('displayNone');
        document.querySelector('.game').classList.add('displayNone');
    }
};
// VIEW USER-------------------------------------------------------

// VIEW GAME-------------------------------------------------------
const viewGame = () => {
    if(state.currentView == 'game'){
        window.alert("YA ESTAS EN MEMEORY");
    } else {
        state.currentView = 'game';
        document.querySelector('.list').classList.add('displayNone');
        document.querySelector('.main').classList.add('displayNone');
        document.querySelector('.add').classList.add('displayNone');
        document.querySelector('.about').classList.add('displayNone');
        document.querySelector('.detail').classList.add('displayNone');
        document.querySelector('.user').classList.add('displayNone');
        document.querySelector('.game').classList.remove('displayNone');
    }
};
// VIEW GAME-------------------------------------------------------




// FUNCTION BUTTONS 'CLICK' EVENT LISTENER-----------------------------
const eventListener = () => {
       listBtn$$.addEventListener('click',viewList);
      mainLogo$$.addEventListener('click',viewMain);
    footerLogo$$.addEventListener('click',viewMain);
        addBtn$$.addEventListener('click',viewAdd);
      aboutBtn$$.addEventListener('click',viewAbout);
       userBtn$$.addEventListener('click',viewUser);
       gameBtn$$.addEventListener('click',viewGame);
}
    //WINDOW ON LOAD
    window.onload = function(){
        eventListener();
        addContent();
        modeSelector();
    }
// FUNCTION BUTTONS 'CLICK' EVENT LISTENER-----------------------------


// BASE DE DATOSSSSSS-------------------------------------------------------------------------------------------------
const memes = [
    {title:'Alumnos del futuro', imgUrl: './MEMES/alumnosFuturo.jpg', description:'Lo van a tener xungo',id:0},
    {title:'Alumnos Online', imgUrl: './MEMES/AlumnosOnline.png', description:'Dejame estoy muerto',id:1},
    {title:'Ataque de risa', imgUrl: './MEMES/ataquederisa.png', description:'La risa en los momentos serios es lo mejor',id:2},
    {title:'Ha acabado la clase', imgUrl: './MEMES/claseHaAcabado.png', description:'Enserio la pregunta ahora BRO?',id:3},
    {title:'Planes confinamiento', imgUrl: './MEMES/confinamiento-1.jpg', description:'Mmmm, si creo que haré eso...',id:4},
    {title:'Daño: Horrible', imgUrl: './MEMES/DañoHorrible.png', description:'Me a destrosado el corason',id:5},
    {title:'Profe vs Dora', imgUrl: './MEMES/Dora.png', description:'Dora es una asignatura del magistrado',id:6},
    {title:'Gmail', imgUrl: './MEMES/Gmail.png', description:'Gmail nos ataca',id:7},
    {title:'Gracias papi', imgUrl: './MEMES/graciaspapa.png', description:'Siempre agradesido papi.',id:8},
    {title:'MindDestroy', imgUrl: './MEMES/Hospital.png', description:'Sigo pensandolo...',id:9},
    {title:'Hulk', imgUrl: './MEMES/Hulk.png', description:'Soy doctor bro',id:10},
    {title:'Juan Cena', imgUrl: './MEMES/JuanCena.png', description:'Si la WWE fuera una novela',id:11},
    {title:'King Kong', imgUrl: './MEMES/KingKong.png', description:'Soy un gorila ¿ok?',id:12},
    {title:'Isla de las Tentaciones', imgUrl: './MEMES/memeIslaTentaciones.jpg', description:'Te pasas tom',id:13},
    {title:'Michisaurus', imgUrl: './MEMES/michisaurus.png', description:'el michisaurus si xiste',id:14},
    {title:'Monstruos SA', imgUrl: './MEMES/MonstruosSA.png', description:'soy un abuelo...',id:15},
    {title:'Papel higienico', imgUrl: './MEMES/papel-higienico-.jpg', description:'que no me falte papel',id:16},
    {title:'Perrito débil', imgUrl: './MEMES/perrodébil.jpg', description:'M da verguensa pidelo tú',id:17},
    {title:'La sintáxis', imgUrl: './MEMES/Sintaxis.png', description:'la sintaxtis ase q vaya en lambo',id:18},
    {title:'Truco USA', imgUrl: './MEMES/trucoUSA.png', description:'que no te pillen el mobil q te expulsan',id:19},
];
// BASE DE DATOSSSSSS-------------------------------------------------------------------------------------------------


// MEMEORY DATA BASE-------------------------------------------------------------------------------------------------
        //EASY
const memeory = [
    {title:'Alumnos del futuro', imgUrl: './MEMES/alumnosFuturo.jpg', description:'Lo van a tener xungo',id:0},
    {title:'Alumnos Online', imgUrl: './MEMES/AlumnosOnline.png', description:'Dejame estoy muerto',id:1},
    {title:'Ataque de risa', imgUrl: './MEMES/ataquederisa.png', description:'La risa en los momentos serios es lo mejor',id:2},
    {title:'Ha acabado la clase', imgUrl: './MEMES/claseHaAcabado.png', description:'Enserio la pregunta ahora BRO?',id:3},
    {title:'Planes confinamiento', imgUrl: './MEMES/confinamiento-1.jpg', description:'Mmmm, si creo que haré eso...',id:4},
    {title:'Daño: Horrible', imgUrl: './MEMES/DañoHorrible.png', description:'Me a destrosado el corason',id:5},
    {title:'Profe vs Dora', imgUrl: './MEMES/Dora.png', description:'Dora es una asignatura del magistrado',id:6},
    {title:'Gmail', imgUrl: './MEMES/Gmail.png', description:'Gmail nos ataca',id:7},
    {title:'Gracias papi', imgUrl: './MEMES/graciaspapa.png', description:'Siempre agradesido papi.',id:8},
    {title:'MindDestroy', imgUrl: './MEMES/Hospital.png', description:'Sigo pensandolo...',id:9},
    {title:'Alumnos del futuro', imgUrl: './MEMES/alumnosFuturo.jpg', description:'Lo van a tener xungo',id:10},
    {title:'Alumnos Online', imgUrl: './MEMES/AlumnosOnline.png', description:'Dejame estoy muerto',id:11},
    {title:'Ataque de risa', imgUrl: './MEMES/ataquederisa.png', description:'La risa en los momentos serios es lo mejor',id:12},
    {title:'Ha acabado la clase', imgUrl: './MEMES/claseHaAcabado.png', description:'Enserio la pregunta ahora BRO?',id:13},
    {title:'Planes confinamiento', imgUrl: './MEMES/confinamiento-1.jpg', description:'Mmmm, si creo que haré eso...',id:14},
    {title:'Daño: Horrible', imgUrl: './MEMES/DañoHorrible.png', description:'Me a destrosado el corason',id:15},
    {title:'Profe vs Dora', imgUrl: './MEMES/Dora.png', description:'Dora es una asignatura del magistrado',id:16},
    {title:'Gmail', imgUrl: './MEMES/Gmail.png', description:'Gmail nos ataca',id:17},
    {title:'Gracias papi', imgUrl: './MEMES/graciaspapa.png', description:'Siempre agradesido papi.',id:18},
    {title:'MindDestroy', imgUrl: './MEMES/Hospital.png', description:'Sigo pensandolo...',id:19},
    
];
        //MEDIUM
const memeoryMedium = [
    {title:'Alumnos del futuro', imgUrl: './MEMES/alumnosFuturo.jpg', description:'Lo van a tener xungo',id:0},
    {title:'Alumnos Online', imgUrl: './MEMES/AlumnosOnline.png', description:'Dejame estoy muerto',id:1},
    {title:'Ataque de risa', imgUrl: './MEMES/ataquederisa.png', description:'La risa en los momentos serios es lo mejor',id:2},
    {title:'Ha acabado la clase', imgUrl: './MEMES/claseHaAcabado.png', description:'Enserio la pregunta ahora BRO?',id:3},
    {title:'Planes confinamiento', imgUrl: './MEMES/confinamiento-1.jpg', description:'Mmmm, si creo que haré eso...',id:4},
    {title:'Daño: Horrible', imgUrl: './MEMES/DañoHorrible.png', description:'Me a destrosado el corason',id:5},
    {title:'Profe vs Dora', imgUrl: './MEMES/Dora.png', description:'Dora es una asignatura del magistrado',id:6},
    {title:'Gmail', imgUrl: './MEMES/Gmail.png', description:'Gmail nos ataca',id:7},
    {title:'Gracias papi', imgUrl: './MEMES/graciaspapa.png', description:'Siempre agradesido papi.',id:8},
    {title:'MindDestroy', imgUrl: './MEMES/Hospital.png', description:'Sigo pensandolo...',id:9},
    {title:'Hulk', imgUrl: './MEMES/Hulk.png', description:'Soy doctor bro',id:10},
    {title:'Juan Cena', imgUrl: './MEMES/JuanCena.png', description:'Si la WWE fuera una novela',id:11},
    {title:'King Kong', imgUrl: './MEMES/KingKong.png', description:'Soy un gorila ¿ok?',id:12},
    {title:'Isla de las Tentaciones', imgUrl: './MEMES/memeIslaTentaciones.jpg', description:'Te pasas tom',id:13},
    {title:'Michisaurus', imgUrl: './MEMES/michisaurus.png', description:'el michisaurus si xiste',id:14},
    {title:'Alumnos del futuro', imgUrl: './MEMES/alumnosFuturo.jpg', description:'Lo van a tener xungo',id:15},
    {title:'Alumnos Online', imgUrl: './MEMES/AlumnosOnline.png', description:'Dejame estoy muerto',id:16},
    {title:'Ataque de risa', imgUrl: './MEMES/ataquederisa.png', description:'La risa en los momentos serios es lo mejor',id:17},
    {title:'Ha acabado la clase', imgUrl: './MEMES/claseHaAcabado.png', description:'Enserio la pregunta ahora BRO?',id:18},
    {title:'Planes confinamiento', imgUrl: './MEMES/confinamiento-1.jpg', description:'Mmmm, si creo que haré eso...',id:19},
    {title:'Daño: Horrible', imgUrl: './MEMES/DañoHorrible.png', description:'Me a destrosado el corason',id:20},
    {title:'Profe vs Dora', imgUrl: './MEMES/Dora.png', description:'Dora es una asignatura del magistrado',id:21},
    {title:'Gmail', imgUrl: './MEMES/Gmail.png', description:'Gmail nos ataca',id:22},
    {title:'Gracias papi', imgUrl: './MEMES/graciaspapa.png', description:'Siempre agradesido papi.',id:23},
    {title:'MindDestroy', imgUrl: './MEMES/Hospital.png', description:'Sigo pensandolo...',id:24},
    {title:'Hulk', imgUrl: './MEMES/Hulk.png', description:'Soy doctor bro',id:25},
    {title:'Juan Cena', imgUrl: './MEMES/JuanCena.png', description:'Si la WWE fuera una novela',id:26},
    {title:'King Kong', imgUrl: './MEMES/KingKong.png', description:'Soy un gorila ¿ok?',id:27},
    {title:'Isla de las Tentaciones', imgUrl: './MEMES/memeIslaTentaciones.jpg', description:'Te pasas tom',id:28},
    {title:'Michisaurus', imgUrl: './MEMES/michisaurus.png', description:'el michisaurus si xiste',id:29},
];
        //HARD
const memeoryHard = [
    {title:'Alumnos del futuro', imgUrl: './MEMES/alumnosFuturo.jpg', description:'Lo van a tener xungo',id:0},
    {title:'Alumnos Online', imgUrl: './MEMES/AlumnosOnline.png', description:'Dejame estoy muerto',id:1},
    {title:'Ataque de risa', imgUrl: './MEMES/ataquederisa.png', description:'La risa en los momentos serios es lo mejor',id:2},
    {title:'Ha acabado la clase', imgUrl: './MEMES/claseHaAcabado.png', description:'Enserio la pregunta ahora BRO?',id:3},
    {title:'Planes confinamiento', imgUrl: './MEMES/confinamiento-1.jpg', description:'Mmmm, si creo que haré eso...',id:4},
    {title:'Daño: Horrible', imgUrl: './MEMES/DañoHorrible.png', description:'Me a destrosado el corason',id:5},
    {title:'Profe vs Dora', imgUrl: './MEMES/Dora.png', description:'Dora es una asignatura del magistrado',id:6},
    {title:'Gmail', imgUrl: './MEMES/Gmail.png', description:'Gmail nos ataca',id:7},
    {title:'Gracias papi', imgUrl: './MEMES/graciaspapa.png', description:'Siempre agradesido papi.',id:8},
    {title:'MindDestroy', imgUrl: './MEMES/Hospital.png', description:'Sigo pensandolo...',id:9},
    {title:'Hulk', imgUrl: './MEMES/Hulk.png', description:'Soy doctor bro',id:10},
    {title:'Juan Cena', imgUrl: './MEMES/JuanCena.png', description:'Si la WWE fuera una novela',id:11},
    {title:'King Kong', imgUrl: './MEMES/KingKong.png', description:'Soy un gorila ¿ok?',id:12},
    {title:'Isla de las Tentaciones', imgUrl: './MEMES/memeIslaTentaciones.jpg', description:'Te pasas tom',id:13},
    {title:'Michisaurus', imgUrl: './MEMES/michisaurus.png', description:'el michisaurus si xiste',id:14},
    {title:'Monstruos SA', imgUrl: './MEMES/MonstruosSA.png', description:'soy un abuelo...',id:15},
    {title:'Papel higienico', imgUrl: './MEMES/papel-higienico-.jpg', description:'que no me falte papel',id:16},
    {title:'Perrito débil', imgUrl: './MEMES/perrodébil.jpg', description:'M da verguensa pidelo tú',id:17},
    {title:'La sintáxis', imgUrl: './MEMES/Sintaxis.png', description:'la sintaxtis ase q vaya en lambo',id:18},
    {title:'Truco USA', imgUrl: './MEMES/trucoUSA.png', description:'que no te pillen el mobil q te expulsan',id:19},
    {title:'Alumnos del futuro', imgUrl: './MEMES/alumnosFuturo.jpg', description:'Lo van a tener xungo',id:20},
    {title:'Alumnos Online', imgUrl: './MEMES/AlumnosOnline.png', description:'Dejame estoy muerto',id:21},
    {title:'Ataque de risa', imgUrl: './MEMES/ataquederisa.png', description:'La risa en los momentos serios es lo mejor',id:22},
    {title:'Ha acabado la clase', imgUrl: './MEMES/claseHaAcabado.png', description:'Enserio la pregunta ahora BRO?',id:23},
    {title:'Planes confinamiento', imgUrl: './MEMES/confinamiento-1.jpg', description:'Mmmm, si creo que haré eso...',id:24},
    {title:'Daño: Horrible', imgUrl: './MEMES/DañoHorrible.png', description:'Me a destrosado el corason',id:25},
    {title:'Profe vs Dora', imgUrl: './MEMES/Dora.png', description:'Dora es una asignatura del magistrado',id:26},
    {title:'Gmail', imgUrl: './MEMES/Gmail.png', description:'Gmail nos ataca',id:27},
    {title:'Gracias papi', imgUrl: './MEMES/graciaspapa.png', description:'Siempre agradesido papi.',id:28},
    {title:'MindDestroy', imgUrl: './MEMES/Hospital.png', description:'Sigo pensandolo...',id:29},
    {title:'Hulk', imgUrl: './MEMES/Hulk.png', description:'Soy doctor bro',id:30},
    {title:'Juan Cena', imgUrl: './MEMES/JuanCena.png', description:'Si la WWE fuera una novela',id:31},
    {title:'King Kong', imgUrl: './MEMES/KingKong.png', description:'Soy un gorila ¿ok?',id:32},
    {title:'Isla de las Tentaciones', imgUrl: './MEMES/memeIslaTentaciones.jpg', description:'Te pasas tom',id:33},
    {title:'Michisaurus', imgUrl: './MEMES/michisaurus.png', description:'el michisaurus si xiste',id:34},
    {title:'Monstruos SA', imgUrl: './MEMES/MonstruosSA.png', description:'soy un abuelo...',id:35},
    {title:'Papel higienico', imgUrl: './MEMES/papel-higienico-.jpg', description:'que no me falte papel',id:36},
    {title:'Perrito débil', imgUrl: './MEMES/perrodébil.jpg', description:'M da verguensa pidelo tú',id:37},
    {title:'La sintáxis', imgUrl: './MEMES/Sintaxis.png', description:'la sintaxtis ase q vaya en lambo',id:38},
    {title:'Truco USA', imgUrl: './MEMES/trucoUSA.png', description:'que no te pillen el mobil q te expulsan',id:39},
];
// MEMEORY DATA BASE-------------------------------------------------------------------------------------------------

//lvl btns
const btnEasyMode$$ = document.querySelector('#easy')
const btnMediumMode$$ = document.querySelector('#medium')
const btnHardMode$$ = document.querySelector('#hard')
//depende del boton que selecciones ejecutamos la funcion para crear el tablero con un parámetro distinto
const modeSelector = () => {
    btnEasyMode$$.addEventListener('click',() => {tableCardsDown(memeory)});
  btnMediumMode$$.addEventListener('click',() => {tableCardsDown(memeoryMedium)});
    btnHardMode$$.addEventListener('click',() => {tableCardsDown(memeoryHard)});
}

// div para insertar el tablero 
const divMode$$ = document.querySelector('[data-function="grid"]'); 

//arrays vacíos para el juego
let clickCard = [];
let cardMatch = [];

// MEMEORY CREATE BOARD -------------------------------------------------------------------------------------------------

function tableCardsDown(array){
    array.sort(() => 0.5 - Math.random()) //array random
    if(array == memeoryMedium || array == memeoryHard){ //array medio o grande cambia tablero
        divMode$$.classList.add('b-grid-medium');
        divMode$$.classList.remove('b-grid');
    }else{
        divMode$$.classList.remove('b-grid-medium');
        divMode$$.classList.add('b-grid');
    }

        //reset game
    cardMatch = [];    
    clickCard = [];
    divMode$$.innerHTML= '';
    attempts$.innerHTML='0';
    score$.innerHTML='0';
        //bucle para crear tablero
    for (let i = 0; i < array.length; i++) {
        const cards = array[i]
        const img$$ = document.createElement('img');
        img$$.setAttribute('data-id', cards.id);
        img$$.setAttribute('src', './Iconos/ME(ME)_expandida.ico');

        img$$.addEventListener('click', ($event) => card($event.target, i,array));

        divMode$$.appendChild(img$$);
    }
}

// MEMEORY MATCH-------------------------------------------------------------------------------------------------

function match (array) {      
    const card1 = clickCard[0]
    const card2 = clickCard[1]

    const card1$ = document.querySelector('[data-id="' + card1.id + '"]')
    const card2$ = document.querySelector('[data-id="' + card2.id + '"]')

    if(card1.id === card2.id){
        card1$.setAttribute('src', './Iconos/ME(ME)_expandida.ico');
        card2$.setAttribute('src', './Iconos/ME(ME)_expandida.ico');
        alert('Es la misma, pakete')
    } else if(card1.id !== card2.id && card1.title === card2.title){
        card1$.setAttribute('src', './Iconos/okay.svg');
        card2$.setAttribute('src', './Iconos/okay.svg');

        card1$.removeEventListener('click', card);
        card2$.removeEventListener('click', card);
        cardMatch.push(card1);
    }else{
        // alert('PAKETE')
        card1$.setAttribute('src', './Iconos/ME(ME)_expandida.ico');
        card2$.setAttribute('src', './Iconos/ME(ME)_expandida.ico');
    }
    clickCard = [];
    score(array);
}

// MEMEORY CLICK MEMORY -------------------------------------------------------------------------------------------------

function card (clickTarget, id,array) {
    const card = array[id]
    const cardPair = cardMatch.find(pair => pair.title === card.title);

    if(cardPair){
        alert('Ya has acertado esta... ¡ANSIAS!')
    }else{
        clickCard.push(card);
        clickTarget.setAttribute('src', card.imgUrl);
        if(clickCard.length ===2){
            setTimeout(()=>{match(array)}, 500);
        }

    }
}

// MEMEORY SCORE -------------------------------------------------------------------------------------------------

const score$ = document.querySelector('[data-function="score"]')
const attempts$ = document.querySelector('[data-function="attempts"]')

function score(array){
    score$.innerHTML = cardMatch.length;
    attempts$.innerHTML = Number(attempts$.innerHTML) + 1;
    if(cardMatch.length === array.length / 2){
        alert('YA ERA HORA... MADRE MÍA')
    }
}